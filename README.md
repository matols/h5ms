# h5ms

1. h5ms，是一个基于H5的 Canvas原生绘制 K线图表库。
主要包括：K线主图、成交量副图、MACD 副图，以及在图中进行的各种点位标记，连线标记。

![输入图片说明](https://gitee.com/uploads/images/2018/0115/144727_ba4846a0_125168.png "152722_QcWp_938910.png")

集成了股票分析中的主要组件，同时对于这些组件进行的简要的封装，以满足非常灵活、多变、细粒度的业务场景的处理。在实际的使用当中，只需要引用js组件即可使用，对于更加个性化的需求，可直接基于js进行二次开发，可控制的力度非常精细。


2. 模拟股票自动化撮合交易过程，以及买卖五档、分时图、K线联动演化全流程 [trades](https://gitee.com/mscore/mt-trades)
![输入图片说明](https://foruda.gitee.com/images/1678266349660245856/f0980b34_125168.png)
![输入图片说明](https://foruda.gitee.com/images/1678266361725880031/0a2d96ec_125168.png)

3. 马桶自定义画板，新增蜡烛图K线组件 [trades](https://gitee.com/mscore/mt-trades)
![输入图片说明](https://gitee.com/mscore/mt-trades/raw/master/WX20230309-143527.png)


4. 体验小程序：
![输入图片说明](gh_40d05b5547dd_430%20(1).jpg)


 **满足你对于股票图表的一切幻想。** 